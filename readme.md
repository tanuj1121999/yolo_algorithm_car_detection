This project implements the YOLO(You Only Look Once) algorithm for autonomous driving - car detection. A pre-trained model is used since
YOLO requires alot of data and takes alot of time. The data is provided by drive.ai. This project is done as a part of deep learning
specialization at deeplearning.ai

Implemented in python using tensorflow and keras.